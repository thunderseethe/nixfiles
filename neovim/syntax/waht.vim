syntax clear

syntax case match

syntax keyword wahtType i32
highlight link wahtType Type

syntax keyword wahtDefn fn
highlight link wahtDefn Function

syntax match wahtIdent /\<[a-z_][a-zA-Z_0-9]*\>/
highlight link wahtIdent Identifier

syntax match wahtComment /#.*/
highlight link wahtComment Comment

syntax region wahtString start=/"/ skip=/\\"/ end=/"/
highlight link wahtString String
