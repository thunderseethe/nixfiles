set ruler
set mouse=a
set hidden
set termguicolors
set splitbelow
set splitright
set nowrap

set shiftwidth=0
set tabstop=2
set softtabstop=2
set expandtab
set smarttab

set number

set shortmess+=c
set updatetime=300
set completeopt=menuone,noinsert,noselect

let $NVIM_TUI_ENABLE_TRUE_COLOR=1
let mapleader=' '

" completion-nvim
" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Telescope keybindings
nnoremap <leader>ff <cmd>Telescope live_grep<cr>
nnoremap <leader>lf <cmd>Telescope lsp_references<cr>
nnoremap <leader>ls <cmd>Telescope lsp_workspace_symbols<cr>

" Trouble keybindings
nnoremap <leader>xx <cmd>TroubleToggle<cr>
nnoremap <leader>xw <cmd>TroubleToggle workspace_diagnostics<cr>
nnoremap <leader>xd <cmd>TroubleToggle document_diagnostics<cr>
nnoremap <leader>xq <cmd>TroubleToggle quickfix<cr>
nnoremap <leader>xl <cmd>TroubleToggle loclist<cr>

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

colorscheme tokyonight-storm
highlight Pmenu guifg=#dcdfe4 guibg=#282c34

" Set filetype for custom lang
au BufRead,BufNewFile *.waht set filetype=waht

function! Scratch()
    noswapfile hide enew
    setlocal buftype=scratch
endfunction

