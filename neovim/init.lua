-- Devicon Config
require 'nvim-web-devicons'.setup {
  default = true
}

-- LSP config

local configs = require 'lspconfig.configs'
configs.waht = {
  default_config = {
    cmd = { '/home/thunderseethe/rs/waht/target/debug/waht' },
    filetypes = { 'waht' },
    root_dir = require('lspconfig.util').find_git_ancestor,
    single_file_support = true,
  },
  docs = {
    description = [[
language server for waht language
    ]],
    default_config = {
      root_dir = [[root_pattern(".git")]],
    },
  },
}
configs.aiahr_anaylzer = {
  default_config = {
    cmd = { '/home/thunderseethe/aiahr/target/release/lsp' },
    filetypes = { 'aiahr' },
    root_dir = require('lspconfig.util').root_pattern('.'),
    single_file_support = true,
  },
  docs = {
    description = [[
Language server for aiahr language
    ]],
    default_config = {
      root_dir = [[root_pattern(".")]],
    },
  },
}
configs.ltex = {
  default_config = {
    cmd = { 'ltex-ls' },
    filetypes = { 'markdown' },
    root_dir = require('lspconfig.util').root_pattern('.'),
    single_file_support = true,
  },
  docs = {
    description = [[
Language server to check spelling and grammar
    ]],
    default_config = {
      root_dir = [[root_pattern(".")]],
    }
  }
}

local lsp=require 'lspconfig'

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr) 
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  --local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.format { async = true }<CR>', opts)
end

-- Setup nvim-cmp.
local lspkind = require'lspkind'

local cmp = require'cmp'
cmp.setup({
  snippet = {
    -- REQUIRED - you must specify a snippet engine
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
      -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
      -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
      -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
    end,
  },
  formatting = {
    format = lspkind.cmp_format({
      with_text = false,
      maxwidth = 50,
      -- before = function(entry, vim_item) ... end
    })
  },
  mapping = {
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
      end
    end, {'i', 's'}),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end, {'i', 's'})
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
  }, {
    { name = 'buffer' },
  })
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
  sources = {
    { name = 'buffer' }
  }
})

-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

lsp.hls.setup({
  on_attach=on_attach, 
  capabilities=capabilities,
  settings = {
    haskell = {
      formattingProvider = "fourmolu",
      plugin = {
        tactics = {
          globalOn = true,
          codeActionsOn = true,
          codeLensOn = true,
          diagnosticsOn = true,
          hoverOn = true,
          symbolsOn = true,
          renameOn = true,
          completionOn = true
        }
      }
    }
  },
})
lsp.rust_analyzer.setup({
  on_attach=on_attach, 
  capabilities=capabilities,
  settings = {
    ["rust-analyzer"] = {
      assist = {
        importGranularity = "module",
        importPrefix = "by_self",
      },
      cargo = {
        loadOutDirsFromCheck = true,
        runBuildScripts = true
      },
      check = {
        command = "clippy"
      },
      procMacro = {
        enable = true
      }, 
    }
  }
})
lsp.tsserver.setup({
  on_attach=on_attach,
  capabilities=capabilities,
})
lsp.wgsl_analyzer.setup({
  on_attach=on_attach,
  capabilities=capabilities,
})
lsp.waht.setup({
  on_attach=on_attach, 
  capabilities=capabilities,
})
lsp.aiahr_anaylzer.setup({
  on_attach=on_attach,
  capabilities=capabilities,
})
lsp.ltex.setup({
  on_attach=on_attach, 
  capabilities=capabilities,
})

-- Treesitter Config
vim.cmd[[au BufRead,BufNewFile *.wgsl set filetype=wgsl]]
vim.cmd[[au BufRead,BufNewFile *.aiahr set filetype=aiahr]]

local parser_config = require"nvim-treesitter.parsers".get_parser_configs()
parser_config.wgsl = {
  install_info = {
    url = "https://github.com/szebniok/tree-sitter-wgsl",
    files = {"src/parser.c"}
  }
}

require 'nvim-treesitter.configs'.setup {
  ensured_installed = {"haskell", "rust", "lua", "nix", "wgsl"},
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn",
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  },
}

vim.wo.foldmethod = "expr"
vim.wo.foldexpr = "nvim_treesitter#foldexpr()"
vim.o.foldlevelstart = 99

-- Bufferline Config
require("bufferline").setup{
  options = {
    numbers = "buffer_id",
    number_style = "superscript",
  }
}

-- Lualine Config
require 'lualine'.setup {
  options = {
    theme = 'dracula'
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {{
      'filename',
      path = 1
    }},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
}

-- Telescope Config
require 'telescope'.setup {}
require('telescope').load_extension('aerial')

require('aerial').setup({
  -- optionally use on_attach to set keymaps when aerial has attached to a buffer
  on_attach = function(bufnr)
    -- Jump forwards/backwards with '{' and '}'
    vim.keymap.set('n', '{', '<cmd>AerialPrev<CR>', {buffer = bufnr})
    vim.keymap.set('n', '}', '<cmd>AerialNext<CR>', {buffer = bufnr})
  end
})
-- You probably also want to set a keymap to toggle aerial
vim.keymap.set('n', '<leader>A', '<cmd>AerialToggle!<CR>')
