{ config, pkgs, ... }:
{
  nixpkgs.overlays = [
    (self: super: {
      fcitx-engines = pkgs.fcitx5;
    })
  ];
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "thunderseethe";
  home.homeDirectory = "/home/thunderseethe";

  
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";

  home.file.".direnvrc".source = ./direnvrc;

  home.packages = (with pkgs; [
    agda
    automake
    autoconf
    bat
    binaryen
    binutils
    coq
    coqPackages.coq-lsp
    coreutils
    exa
    fd
    fzf
    gcc
    gh
    ghc
    gnumake
    htop
    lean
    ltex-ls
    mold
    ripgrep
    unzip
    webfs
    wget
    xsel
  ]);

  programs.direnv = {
    enable = true;  
    nix-direnv = {
      enable = true;
    };
  };

  programs.bash = {
    enable = true;
    initExtra = ''
PROMPT_COLOR="1;35m"
PS1="\n\[\033[$PROMPT_COLOR\][\u@\h:\w]\\$\[\033[0m\] "
EDITOR=nvim
    '';
    profileExtra = ''
if [ -e "$HOME/.ghcup/bin" ] ; then
    PATH="$HOME/.ghcup/bin:$PATH"
fi
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
if [ -e /home/thunderseethe/.nix-profile/etc/profile.d/nix.sh ]; then . /home/thunderseethe/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
    '';
    shellAliases = {
      ls = "exa";
    };
  };

  programs.fish = {
    enable = true;
  };

  programs.git = {
    enable = true;
    userName = "thunderseethe";
    userEmail = "thunderseethe.dev@gmail.com";
    extraConfig = {
      pull = {rebase = true;};
      core = {editor = "nvim";};
    };
  };

  home.file.".config/nvim/syntax/waht.vim".source = ./neovim/syntax/waht.vim;
  home.file.".config/nvim/syntax/wgsl.vim".source = ./neovim/syntax/wgsl.vim;
  home.file.".config/nvim/syntax/koka.vim".source = ./neovim/syntax/koka.vim;
  programs.neovim = {
    enable = true;
    #package = pkgs.neovim-nightly;
    vimdiffAlias = true;
    extraConfig = builtins.readFile ./neovim/init.vim + 
    ''
    lua << EOF
    ${builtins.readFile ./neovim/init.lua}
    EOF
    '';

    extraPackages =  with pkgs; [ tree-sitter ];
    plugins = (with pkgs.vimPlugins; [
      aerial-nvim
      lualine-nvim
      bufferline-nvim
      nvim-web-devicons
      onehalf

      vim-nix
      vim-addon-nix
      plenary-nvim
      (nvim-treesitter.withPlugins (
        p: [
          p.tree-sitter-lua
          p.tree-sitter-haskell
          p.tree-sitter-nix
          p.tree-sitter-rust
        ]))

      # LSP stuff
      nvim-lspconfig
      lsp-colors-nvim
      lspkind-nvim
      telescope-nvim
      # completion
      nvim-cmp
      cmp-nvim-lsp
      cmp-buffer
      cmp-path
      # snippet engine
      cmp-vsnip
      vim-vsnip

      agda-vim
      Coqtail
      haskell-vim
      lean-nvim
      vim-hoogle
      idris2-vim
      tokyonight-nvim
      trouble-nvim
    ]);
  };

  programs.tmux = {
    enable = true;
    terminal = "screen-256color";
    clock24 = true;
    shortcut = "a";
    keyMode = "vi";
    sensibleOnTop = true;
    plugins = with pkgs; [
      tmuxPlugins.pain-control
      tmuxPlugins.copycat
      tmuxPlugins.yank
      tmuxPlugins.logging
      tmuxPlugins.onedark-theme
    ];
    extraConfig = ''
set-option -sg escape-time 10
set-option -sa terminal-overrides ',XXX:RGB'
set-option -ga terminal-overrides ",xterm-256color:Tc"

# Undercurl
set -g default-terminal "xterm-256color"
set -g mouse on
set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'  # undercurl support
set -as terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'  # underscore colours - needs tmux-3.0
    '';
  };
}

